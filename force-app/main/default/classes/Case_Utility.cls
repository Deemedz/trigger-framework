public with sharing class Case_Utility 
{
    private static final String HANDLER_NAME = 'Case_Trigger_Handler';

    public static void updateCaseSubject(List<Case> caseList)
    {
        for(Case cs: caseList)
        {
            cs.Subject = 'Kewl New Subject';
        }
    }
    
    public static void createCase()
    {
        TriggerHandler.bypass(HANDLER_NAME);

        Case newCase = new Case();
        newCase.Subject = 'Awesome Case';
        insert newCase;

        TriggerHandler.clearBypass(HANDLER_NAME);

    }


    // public static void updateDescription(List<Case> caseDesList)
    // {
    //     for(Case cs: caseDesList)
    //     {
    //         cs.Subject = 'Kewl New Subject';
    //     }
    // }

    // public static void caseRecovery(List<Case> casesUnDeleted = new List<Case>();)
    // {
    //         casesUnDeleted = [SELECT Id, Origin, CaseNumber FROM Case WHERE Id IN :trigger.newMap.keyset()];
            
    //         for(Case c :casesUnDeleted) {
    //             if(c.Type.equals('Structural')){
    //                 update casesUnDeleted;
    //             }
    //         }
        
    // }

    /*MY ADDED METHODS FOR PRACTICE*/
    //  public static void delCaseList()
    //  {
    //      if(
    //          Case.SObjectType.getDescribe().isAccessible() &&
    //          Schema.SObjectType.Contact.fields.Name.isAccessible() &&
    //          Schema.SObjectType.Contact.fields.Phone.isAccessible()
    //      )
    //      {
    //          for(Case ca : [SELECT Id FROM Case 
    //          WHERE Id IN (SELECT CaseId FROM Account) AND 
    //          Id =: Trigger.old]){
    //              Trigger.oldMap.get(ca.Id).addError('Case Cannot be deleted');
    //         }
    //      }
    //  }
}