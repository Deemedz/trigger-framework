public with sharing class Account_Trigger_Handler extends TriggerHandler {
    public override void beforeDelete() {
        Account_Utility.barDeletion();
        
    }

    public override void beforeInsert(){
        Account_Utility.setRating(trigger.new);
    }

    public override void afterDelete(){
        Account_Utility.deleteNotification(trigger.old);
    }
}
