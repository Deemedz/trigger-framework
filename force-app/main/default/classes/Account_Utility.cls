public with sharing class Account_Utility {

    //Constant variable for the string name of the trigger handler.
    private static final String ACCOUNT_HANDLER_NAME = 'Account_Trigger_Handler';
    
    public static void barDeletion(){
        if(
            Account.SObjectType.getDescribe().isAccessible() &&
            Schema.SObjectType.Contact.fields.Name.isAccessible() &&
            Schema.SObjectType.Contact.fields.Phone.isAccessible()
        )
        {
            for (Account a : [SELECT Id FROM Account
                     WHERE Id IN (SELECT AccountId FROM Opportunity) AND
                     Id IN :Trigger.old]) {
                    Trigger.oldMap.get(a.Id).addError(
                    'Cannot delete account with related opportunities. Remove the opportunity first');
            }
        }
    }




    public static void setRating(List<Account> accounts)
    {
        for(Account record:accounts){
            if(record.Rating.equals('Cold'))
            {
                record.Description = 'new account from new trigger';
            }
        }
    }


    public static void deleteNotification(List<Account> accounts){
        Messaging.reserveSingleEmailCapacity(trigger.size);
        List<Messaging.SingleEmailMessage> email= new List<Messaging.SingleEmailMessage>();
        for(Account record:accounts){
            
            // if(Trigger.oldMap.get(record.Id)){
            //     Trigger.oldMap.get(record.Id).message('Account Deleted Successfully');
            // }

        }
    }
}
