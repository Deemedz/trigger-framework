public with sharing class Case_Trigger_Handler extends TriggerHandler 
{
    public override void beforeInsert()
    {
        Case_Utility.updateCaseSubject(trigger.new);
        Case_Utility.createCase();
        // Case_Utility.delCaseList();
    }    

    // public override void afterDelete()
    // {
    //     Case_Utility.caseRecovery(trigger.new);
    // }

    // public override void afterUpdate(){
    //     Case_Utility.updateDescription(trigger.new);
    // }
}