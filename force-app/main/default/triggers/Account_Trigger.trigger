trigger Account_Trigger on Account (before delete, before insert, after delete) 
{
    new Account_Trigger_Handler().run();
}